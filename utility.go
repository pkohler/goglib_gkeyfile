package gkeyfile

import (
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"github.com/cloudfoundry/jibber_jabber"
)

func GGetLanguageNames() (string, error) {
	/*
	 * Obtain a string containing valid locales.
	 * We'll just use a 3rd party lib for now
	 */
	lang, err := jibber_jabber.DetectLanguage()
	return lang, err
}

func findFileInDataDirs(filename string, folderpaths []string) (os.FileInfo, string, error) {
	/*
	 * Iterates through all the directories in *dirs trying to
	 * open file.  When it successfully locates and opens a file it
	 * returns the file descriptor to the open file.  It also
	 * outputs the absolute path of the file in output_file.
	 */
	var err error
	for _, dir := range folderpaths {
		contents, err := walkDir(dir)
		if err != nil {
			return nil, "", err
		}
		for _, item := range contents {
			if item[1] == filename {
				fullpath := filepath.Join(item[0], item[1])
				descr, err := os.Lstat(fullpath)
				if err != nil {
					return nil, "", err
				}
				return descr, fullpath, nil
			}
		}
	}
	err = fmt.Errorf(
		"%v - G_KEY_FILE_ERROR_NOT_FOUND - "+
			"Valid key file could not be found in search dirs [%s]",
		g_KEY_FILE_ERROR_NOT_FOUND,
		filename,
		strings.Join(folderpaths, ", "),
	)
	return nil, "", err
}

func walkDir(folderpath string) ([][]string, error) {
	/*
	 * recursively walks through a folder, returning a list of
	 * [directory, fileOrFolderName] strings
	 */
	if current, err := os.Open(folderpath); err != nil {
		return nil, err
	} else {
		out := [][]string{}
		contents, err := current.Readdirnames(-1)
		if err != nil {
			return nil, err
		}
		var joined string
		for _, item := range contents {
			out = append(out, []string{folderpath, item})
			joined = filepath.Join(folderpath, item)
			stat, err := os.Lstat(joined)
			if err != nil {
				return out, err
			}
			if stat.IsDir() {
				subcontents, err := walkDir(joined)
				if err != nil {
					return out, err
				}
				out = append(out, subcontents...)
			}
		}
		return out, nil
	}
}

func lineIsComment(line string) bool {
	return reIsComment.MatchString(line)
}

func lineIsGroup(line string) bool {
	return reIsGroup.MatchString(line)
}

func lineIsKeyValuePair(line string) bool {
	return reIsKeyValuePair.MatchString(line)
}

func isGroupName(group string) bool {
	// group names can be any valid UTF8 string
	return group != ""
}

func isKeyName(key string) bool {
	if key == "" {
		// no empty keys, please
		return false
	}

	if key != strings.TrimSpace(key) {
		/*
		 * We accept spaces in the middle of keys to not break
		 * existing apps, but we don't tolerate initial or final
		 * spaces, which would lead to silent corruption when
		 * rereading the file.
		 */
		return false
	}

	locale := keyGetLocale(key)
	if locale != "" {
		/*
		 * Checks to make sure the locale only contains valid characters;
		 * doesn't actually check to see if it's a real locale or not.
		 */
		if !reValidLocale.MatchString(locale) {
			return false
		}
	}
	return true
}

func keyGetLocale(key string) string {
	return reGroupOrLocaleName.FindString(key)
}
