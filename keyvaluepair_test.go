package gkeyfile

import "testing"

var valtypes = []GKeyFileValueType{
	G_KEY_FILE_TYPE_STR,
	G_KEY_FILE_TYPE_INT,
	G_KEY_FILE_TYPE_BOOL,
	G_KEY_FILE_TYPE_STR_LIST,
	G_KEY_FILE_TYPE_INT_LIST,
	G_KEY_FILE_TYPE_BOOL_LIST,
}

func newKVP(valType GKeyFileValueType) *GKeyFileKeyValuePair {
	var val interface{}
	switch valType {
	case G_KEY_FILE_TYPE_STR:
		val = "stringy"
	case G_KEY_FILE_TYPE_INT:
		val = int64(42)
	case G_KEY_FILE_TYPE_BOOL:
		val = true
	case G_KEY_FILE_TYPE_STR_LIST:
		val = []string{"jabberwocky", "bandersnatch"}
	case G_KEY_FILE_TYPE_INT_LIST:
		val = []int64{1337, 9001}
	case G_KEY_FILE_TYPE_BOOL_LIST:
		val = []bool{false, true}
	default:
		val = "stringy"
		valType = G_KEY_FILE_TYPE_STR
	}
	return &GKeyFileKeyValuePair{
		Key:     "test",
		Value:   val,
		valType: valType,
	}
}

func newKVPFromString(valType GKeyFileValueType) *GKeyFileKeyValuePair {
	var val interface{}
	switch valType {
	case G_KEY_FILE_TYPE_STR:
		val = "stringy"
	case G_KEY_FILE_TYPE_INT:
		val = "42"
	case G_KEY_FILE_TYPE_BOOL:
		val = "true"
	case G_KEY_FILE_TYPE_STR_LIST:
		val = "jabberwocky;bandersnatch"
	case G_KEY_FILE_TYPE_INT_LIST:
		val = "1337;9001"
	case G_KEY_FILE_TYPE_BOOL_LIST:
		val = "false;true"
	default:
		val = "stringy"
		valType = G_KEY_FILE_TYPE_STR
	}
	return &GKeyFileKeyValuePair{
		Key:     "test",
		Value:   val,
		valType: G_KEY_FILE_TYPE_STR,
	}
}

func TestValidate(t *testing.T) {
	for _, v := range valtypes {
		k := newKVP(v)
		ty, err := k.Validate(";")
		if ty != v {
			t.Errorf(
				"Validation failed for %v; got type %d when %d was expected",
				k,
				ty,
				v,
			)
		}
		if err != nil {
			t.Errorf("Error validating %v: %s", k, err.Error())
		}
	}
}

func TestParse(t *testing.T) {
	for _, v := range valtypes {
		k := newKVPFromString(v)
		err := k.ParseType(";")
		if k.valType != v {
			t.Errorf(
				"ParseType failed for %v; got type %d when %d was expected",
				k,
				k.valType,
				v,
			)
		}
		if err != nil {
			t.Errorf("Error parsing type for %v: %s", k, err.Error())
		}
		// make sure there's no panics trying to stringify anything
		_ = k.String()
		_ = k.ValueAsString(";")
	}
}

func TestGetValue(t *testing.T) {
	for _, v := range valtypes {
		k := newKVP(v)
		var err error
		switch k.valType {
		case G_KEY_FILE_TYPE_STR:
			_, err = k.GetStringValue()
		case G_KEY_FILE_TYPE_INT:
			_, err = k.GetIntValue()
		case G_KEY_FILE_TYPE_BOOL:
			_, err = k.GetBoolValue()
		case G_KEY_FILE_TYPE_STR_LIST:
			_, err = k.GetStringList()
		case G_KEY_FILE_TYPE_INT_LIST:
			_, err = k.GetIntList()
		case G_KEY_FILE_TYPE_BOOL_LIST:
			_, err = k.GetBoolList()
		}
		if err != nil {
			t.Errorf("Error getting value of %v: %s", k, err.Error())
		}
		if k.valType != G_KEY_FILE_TYPE_BOOL {
			_, err = k.GetBoolValue()
			if err == nil {
				t.Errorf("Shouldn't be able to get a bool from type %v", k.valType)
			}
		} else {
			_, err = k.GetIntList()
			if err == nil {
				t.Error("Shouldn't be able to get an intlist from a bool")
			}
		}

	}
}
