package gkeyfile

import (
	"fmt"
	"regexp"
	"strconv"
	"strings"
)

type GKeyFileKeyValuePair struct {
	Key     string
	Value   interface{}
	valType GKeyFileValueType
}

func (pair *GKeyFileKeyValuePair) Validate(listSeparator string) (GKeyFileValueType, error) {
	var ok bool
	switch pair.valType {
	case G_KEY_FILE_TYPE_STR:
		_, ok = pair.Value.(string)
	case G_KEY_FILE_TYPE_INT:
		_, ok = pair.Value.(int64)
	case G_KEY_FILE_TYPE_BOOL:
		_, ok = pair.Value.(bool)
	case G_KEY_FILE_TYPE_STR_LIST:
		_, ok = pair.Value.([]string)
	case G_KEY_FILE_TYPE_INT_LIST:
		_, ok = pair.Value.([]int64)
	case G_KEY_FILE_TYPE_BOOL_LIST:
		_, ok = pair.Value.([]bool)
	default:
		panic(
			fmt.Sprintf(
				"unknown GKeyFileValueType (%v) for %v",
				pair.valType,
				pair.Value,
			),
		)
	}
	if ok {
		return pair.valType, nil
	} else {
		// try parsing the value as a string
		pair.valType = G_KEY_FILE_TYPE_STR
		pair.Value = fmt.Sprintf("%v", pair.Value)
		var err error
		//err = pair.ParseType(listSeparator)
		if err != nil {
			return 0, err
		}
		return pair.valType, nil
	}
}

func (pair *GKeyFileKeyValuePair) ParseType(listSeparator string) error {
	v, ok := pair.Value.(string)
	if !ok {
		return GKFErrInvalidValue
	}
	if strings.Contains(v, listSeparator) && !strings.Contains(v, "\\"+listSeparator) {
		// it's a list of some kind since it's got an unescaped separator
		reIsBoolList, err := regexp.Compile(
			fmt.Sprintf(
				`^((true|false)%s?)*$`,
				listSeparator,
			),
		)
		if err != nil {
			return err
		}
		reIsIntList, err := regexp.Compile(
			fmt.Sprintf(
				`^(-?\d+%s?)*$`,
				listSeparator,
			),
		)
		if err != nil {
			return err
		}
		if reIsBoolList.MatchString(v) {
			// should be a list of booleans
			blist := []bool{}
			for _, b := range strings.Split(v, listSeparator) {
				val, err := strconv.ParseBool(b)
				if err != nil {
					return fmt.Errorf(
						"Value `%v` appeared to be a list of bools, but parsing it as such returned error %s",
						v,
						err.Error(),
					)
				}
				blist = append(blist, val)
			}
			pair.Value = blist
			pair.valType = G_KEY_FILE_TYPE_BOOL_LIST
			return nil
		} else if reIsIntList.MatchString(v) {
			// should be a list of integers
			ilist := []int64{}
			for _, i := range strings.Split(v, listSeparator) {
				val, err := strconv.ParseInt(i, 10, 64)
				if err != nil {
					return fmt.Errorf(
						"Value `%v` appeared to be a list of ints, but parsing it as such returned error %s",
						v,
						err.Error(),
					)
				}
				ilist = append(ilist, val)
			}
			pair.Value = ilist
			pair.valType = G_KEY_FILE_TYPE_INT_LIST
			return nil
		} else {
			// must be a list of strings
			slist := strings.Split(v, listSeparator)
			pair.Value = slist
			pair.valType = G_KEY_FILE_TYPE_STR_LIST
			return nil
		}
	}
	// got this far, it's got to be a single bool/int/str value
	b, err := strconv.ParseBool(v)
	if err == nil {
		pair.Value = b
		pair.valType = G_KEY_FILE_TYPE_BOOL
		return nil
	}
	i, err := strconv.ParseInt(v, 10, 64)
	if err == nil {
		pair.Value = i
		pair.valType = G_KEY_FILE_TYPE_INT
		return nil
	}
	pair.Value = v
	pair.valType = G_KEY_FILE_TYPE_STR
	return nil
}

func (pair *GKeyFileKeyValuePair) String() string {
	/*
	 * Default string representation of a GKeyFileKeyValuePair.
	 * Assumes that the list separator is the default ";"
	 */
	return pair.ToString(";")
}

func (pair *GKeyFileKeyValuePair) ToString(listSeparator string) string {
	if pair.valType == G_KEY_FILE_TYPE_STR {
		if pair.Value.(string) == "" {
			return ""
		} else if pair.Key == "" {
			return pair.Value.(string)
		} else {
			return fmt.Sprintf("%s=%s", pair.Key, pair.Value.(string))
		}
	} else {
		return fmt.Sprintf("%s=%s", pair.Key, pair.ValueAsString(listSeparator))
	}
}

func (pair *GKeyFileKeyValuePair) ValueAsString(listSeparator string) string {
	switch pair.valType {
	case G_KEY_FILE_TYPE_STR:
		return pair.Value.(string)
	case G_KEY_FILE_TYPE_INT:
		return fmt.Sprintf("%d", pair.Value.(int64))
	case G_KEY_FILE_TYPE_BOOL:
		return fmt.Sprintf("%v", pair.Value.(bool))
	case G_KEY_FILE_TYPE_STR_LIST:
		strList := pair.Value.([]string)
		return strings.Join(strList, listSeparator)
	case G_KEY_FILE_TYPE_INT_LIST:
		intList := pair.Value.([]int64)
		strList := ""
		for i, v := range intList {
			strList += fmt.Sprintf("%d", v)
			if i < (len(intList) - 1) {
				strList += listSeparator
			}
		}
		return strList
	case G_KEY_FILE_TYPE_BOOL_LIST:
		boolList := pair.Value.([]bool)
		strList := ""
		for i, v := range boolList {
			strList += fmt.Sprintf("%v", v)
			if i < (len(boolList) - 1) {
				strList += listSeparator
			}
		}
		return strList
	default:
		panic(
			fmt.Sprintf(
				"unknown GKeyFileValueType (%v) for %v",
				pair.valType,
				pair.Value,
			),
		)
	}
}

func (pair *GKeyFileKeyValuePair) GetStringValue() (string, error) {
	if pair.valType != G_KEY_FILE_TYPE_STR {
		return "", GKFErrInvalidValue
	}
	return pair.Value.(string), nil
}

func (pair *GKeyFileKeyValuePair) GetIntValue() (int64, error) {
	if pair.valType != G_KEY_FILE_TYPE_INT {
		return 0, GKFErrInvalidValue
	}
	return pair.Value.(int64), nil
}

func (pair *GKeyFileKeyValuePair) GetBoolValue() (bool, error) {
	if pair.valType != G_KEY_FILE_TYPE_BOOL {
		return false, GKFErrInvalidValue
	}
	return pair.Value.(bool), nil
}

func (pair *GKeyFileKeyValuePair) GetStringList() ([]string, error) {
	if pair.valType != G_KEY_FILE_TYPE_STR_LIST && pair.valType != G_KEY_FILE_TYPE_STR {
		return []string{}, GKFErrInvalidValue
	}
	if pair.valType == G_KEY_FILE_TYPE_STR {
		return []string{pair.Value.(string)}, nil
	}
	return pair.Value.([]string), nil
}

func (pair *GKeyFileKeyValuePair) GetIntList() ([]int64, error) {
	if pair.valType != G_KEY_FILE_TYPE_INT_LIST && pair.valType != G_KEY_FILE_TYPE_INT {
		return []int64{}, GKFErrInvalidValue
	}
	if pair.valType == G_KEY_FILE_TYPE_INT {
		return []int64{pair.Value.(int64)}, nil
	}
	return pair.Value.([]int64), nil
}

func (pair *GKeyFileKeyValuePair) GetBoolList() ([]bool, error) {
	if pair.valType != G_KEY_FILE_TYPE_BOOL_LIST && pair.valType != G_KEY_FILE_TYPE_BOOL {
		return []bool{}, GKFErrInvalidValue
	}
	if pair.valType == G_KEY_FILE_TYPE_BOOL {
		return []bool{pair.Value.(bool)}, nil
	}
	return pair.Value.([]bool), nil
}
