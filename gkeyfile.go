package gkeyfile

import (
	"bufio"
	"fmt"
	"os"
	"strings"

	"golang.org/x/text/language"
)

type GKeyFile struct {
	/*
	 * GKeyFile:
	 *
	 * The GKeyFile struct contains only private data
	 * and should not be accessed directly.
	 */
	groups        []*GKeyFileGroup
	groupHash     map[string]*GKeyFileGroup
	startGroup    *GKeyFileGroup
	currentGroup  *GKeyFileGroup
	parseBuffer   string
	listSeparator string
	flags         GKeyFileFlags
	locales       []string
}

func (keyfile *GKeyFile) Init() {
	/*
	 * initialize the GKeyFile with default values
	 * G_KEY_FILE_NONE for flags;
	 * the user's locale for locales,
	 * ";" as the list separator
	 */
	egroup := new(GKeyFileGroup)
	keyfile.startGroup = egroup
	keyfile.currentGroup = egroup
	keyfile.groups = *new([]*GKeyFileGroup)
	keyfile.groupHash = map[string]*GKeyFileGroup{}
	keyfile.parseBuffer = ""
	keyfile.listSeparator = ";"
	keyfile.flags = G_KEY_FILE_NONE
	lang, _ := GGetLanguageNames()
	keyfile.locales = strings.Split(lang, " ")
}

func (keyfile *GKeyFile) Clear() {
	egroup := new(GKeyFileGroup)
	var egroups []*GKeyFileGroup
	var egrouphash map[string]*GKeyFileGroup
	var elocales []string
	keyfile.groups = egroups
	keyfile.groupHash = egrouphash
	keyfile.currentGroup = egroup
	keyfile.startGroup = egroup
	keyfile.parseBuffer = ""
	keyfile.listSeparator = ";"
	keyfile.flags = G_KEY_FILE_NONE
	keyfile.locales = elocales
}

func NewGKeyFile() *GKeyFile {
	/*
	 * Creates a new empty GKeyFile object.
	 * Use a different function if you want to load an existing file
	 */
	g := &GKeyFile{}
	g.Init()
	return g
}

func (keyfile *GKeyFile) SetListSeperator(newSeparator string) {
	/*
	 * Sets the character which is used to separate
	 * values in lists. Typically ';' or ',' are used
	 * as separators. The default list separator is ';'.
	 */
	keyfile.listSeparator = newSeparator
}

func (keyfile *GKeyFile) LoadFromFD(fd os.FileInfo, flags GKeyFileFlags) error {
	/*
	 * Not implemented in Go because os.FileInfo.Name() won't provide full paths.
	 * Use LoadFromFile instead.
	 */
	return GKFErrNotImplemented
}

func (keyfile *GKeyFile) LoadFromFile(filepath string, flags GKeyFileFlags) error {
	/*
	 * g_key_file_load_from_file:
	 *
	 * Loads a key file into an empty or existing #GKeyFile structure.
	 *
	 * If the file was loaded successfully, nil is returned, otherwise the
	 * relevant error is passed up.
	 */
	file, err := os.Open(filepath)
	if err != nil {
		return err
	}
	defer file.Close()
	lsep := keyfile.listSeparator // keep even after clearing the keyfile
	keyfile.Clear()
	keyfile.Init()
	keyfile.listSeparator = lsep // restore the configured listSeperator
	keyfile.flags = flags
	scanner := bufio.NewScanner(file)
	for scanner.Scan() { // read file one line at a time
		if scanner.Err() != nil {
			return scanner.Err()
		}
		err = keyfile.parseData(scanner.Text())
		if err != nil {
			return err
		}
	}
	return nil
}

func (keyfile *GKeyFile) LoadFromData(data string, flags GKeyFileFlags) error {
	/*
	 * Loads a key file from memory into an empty #GKeyFile structure.
	 * If the object cannot be created then %error is set to a #GKeyFileError.
	 */
	var err error
	lsep := keyfile.listSeparator // keep even after clearing the keyfile
	keyfile.Clear()
	keyfile.Init()
	keyfile.listSeparator = lsep // restore the configured listSeperator
	keyfile.flags = flags
	lines := strings.Split(data, "\n")
	for _, line := range lines {
		err = keyfile.parseData(line)
		if err != nil {
			return err
		}
	}
	return nil
}

func (keyfile *GKeyFile) LoadFromDirs(filename string, dirs []string, flags GKeyFileFlags) error {
	/*
	 * This function looks for a key file named @file in the paths
	 * specified in @search_dirs, loads the file into @key_file and
	 * returns the file's full path in @full_path.  If the file could not
	 * be loaded then an %error is set to either a #GFileError or
	 * #GKeyFileError.
	 */
	_, fullPath, err := findFileInDataDirs(filename, dirs)
	if err != nil {
		return err
	}
	err = keyfile.LoadFromFile(fullPath, flags)
	return err
}

func (keyfile *GKeyFile) LoadFromDataDirs(filename string, flags GKeyFileFlags) error {
	/* This function looks for a key file named @file in the paths
	 * returned from g_get_user_data_dir() and g_get_system_data_dirs(),
	 * loads the file into @key_file and returns the file's full path in
	 * @full_path.  If the file could not be loaded then an %error is
	 * set to either a #GFileError or #GKeyFileError.
	 */
	// not implemented in the Go port of gkeyfile.c, use LoadFromDirs instead
	return GKFErrNotImplemented
}

func (keyfile *GKeyFile) parseData(line string) error {
	/*
	 * This function combines g_key_file_parse_data,
	 * g_key_file_flush_parse_buffer and g_key_file_parse_line
	 * due to differences in implementation from glib's gkeyfile.c
	 */
	var err error
	line = strings.TrimSpace(line)
	// only comments are allowed in the start group
	if keyfile.currentGroup == keyfile.startGroup && lineIsKeyValuePair(line) {
		return fmt.Errorf(
			GKFErrInvalidValue.Error()+
				" - Key file contains non-comment key-value pair"+
				" `%s` before the first named group",
			line,
		)
	}
	// blank lines count as comments
	if lineIsComment(line) || line == "" {
		err = keyfile.parseComment(line)
	} else if lineIsGroup(line) {
		err = keyfile.parseGroup(line)
	} else if lineIsKeyValuePair(line) {
		err = keyfile.parseKeyValuePair(line)
	} else {
		err = fmt.Errorf(
			GKFErrParse.Error()+" - Key file contains line '%s' "+
				"which is not a key-value pair, group, or comment",
			line,
		)
	}
	return err
}

func (keyfile *GKeyFile) parseComment(line string) error {
	if keyfile.flags&G_KEY_FILE_KEEP_COMMENTS == 0 {
		// don't store the comment if the flag isn't set
		return nil
	}
	pair := GKeyFileKeyValuePair{
		Key:   "",
		Value: line,
	}
	// add the comment to the current group's kvp slice
	keyfile.currentGroup.KeyValuePairs = append(
		keyfile.currentGroup.KeyValuePairs,
		&pair,
	)
	// don't add it to the lookup dict because comments all use the same key ("")
	return nil
}

func (keyfile *GKeyFile) parseGroup(line string) error {
	var err error
	/*
	 * submatch[1] of reGroupOrLocaleName is the group name without the
	 * enclosing square brackets []
	 */
	groupName := reGroupOrLocaleName.FindStringSubmatch(line)[1]
	if !isGroupName(groupName) {
		err = fmt.Errorf(
			GKFErrParse.Error()+" - Invalid group name: '%s'", groupName,
		)
	} else {
		keyfile.AddGroup(groupName)
	}
	return err
}

func (keyfile *GKeyFile) parseKeyValuePair(line string) error {
	var err error
	var key, value string
	if keyfile.currentGroup == keyfile.startGroup {
		err = fmt.Errorf(
			GKFErrGroupNotFound.Error() +
				" - Key file does not start with a valid group",
		)
		return err
	}
	// split key/value by the first instance of "="
	splitline := strings.SplitN(line, "=", 2)
	key = splitline[0]
	value = splitline[1]
	if !isKeyName(key) {
		err = fmt.Errorf(
			GKFErrParse.Error()+
				" - Invalid key name: %s",
			key,
		)
		return err
	}

	/*
	 * Skipping check to see if the encoding is right since bufio.Scanner should
	 * have done that already
	 */

	// Is this key a translation? If so, is it one that we care about?
	locale := keyGetLocale(key)
	if locale == "" || keyfile.localeIsInteresting(locale) {
		pair := &GKeyFileKeyValuePair{
			Key:   key,
			Value: value,
		}
		err = pair.ParseType(keyfile.listSeparator)
		if err != nil {
			return err
		}
		keyfile.currentGroup.AddKeyValuePair(pair)
	}
	return err
}

func (keyfile *GKeyFile) localeIsInteresting(locale string) bool {
	/*
	 * If G_KEY_FILE_KEEP_TRANSLATIONS is not set, only returns
	 * true for locales that have a High or Exact match with one of
	 * keyfile.locales according to golang.org/x/text/languages
	 */
	if keyfile.flags&G_KEY_FILE_KEEP_TRANSLATIONS > 0 {
		// all locales are interesting if we're keeping translations
		return true
	}
	lang := language.Make(locale)
	interestingLangs := []language.Tag{}
	for _, loc := range keyfile.locales {
		interestingLangs = append(interestingLangs, language.Make(loc))
	}
	matcher := language.NewMatcher(interestingLangs)
	_, _, confidence := matcher.Match(lang)
	if confidence == language.High || confidence == language.Exact {
		// should be an interesting language
		return true
	} else {
		return false
	}
}

func (keyfile *GKeyFile) AddGroup(groupName string) (*GKeyFileGroup, error) {
	/*
	 * Adds groupName as a group to the keyfile and sets it as the currentGroup.
	 * If group already exists, just sets that as the currentGroup.
	 */
	if !isGroupName(groupName) {
		return nil, GKFErrInvalidValue
	}
	var group *GKeyFileGroup
	group = keyfile.LookupGroup(groupName)
	if group != nil {
		// group already exists
		keyfile.currentGroup = group
		return group, nil
	}
	// initialize new group
	group = &GKeyFileGroup{
		Name:          groupName,
		LookupMap:     map[string]*GKeyFileKeyValuePair{},
		KeyValuePairs: []*GKeyFileKeyValuePair{},
	}
	keyfile.groups = append(keyfile.groups, group)
	keyfile.currentGroup = group
	keyfile.groupHash[groupName] = group
	return group, nil
}

func (keyfile *GKeyFile) LookupGroup(groupName string) *GKeyFileGroup {
	return keyfile.groupHash[groupName]
}

/*
func (keyfile *GKeyFile) Ref() {
	// Increases the reference count of keyfile
	// not implemented in the Go port; appears to be used for C garbage collection
}

func (keyfile *GKeyFile) UnRef() {
	// Decreases the reference count of keyfile
	// not implemented in the Go port; appears to be used for C garbage collection
}
*/

func (keyfile *GKeyFile) ToData() string {
	/*
	 * g_key_file_to_data:
	 *
	 * This function outputs @key_file as a string.
	 * Returns: a newly allocated string holding
	 * the contents of the #GKeyFile
	 */
	return keyfile.String()
}

func (keyfile *GKeyFile) String() string {
	str := ""
	str += keyfile.startGroup.ToString(keyfile.listSeparator)
	str += "\n"
	for i, group := range keyfile.groups {
		str += group.ToString(keyfile.listSeparator)
		if i < (len(keyfile.groups) - 1) {
			str += "\n"
		}
	}
	return str
}

func (keyfile *GKeyFile) GetKeys(groupName string) ([]string, error) {
	var keys []string
	if keyfile.groupHash[groupName] == nil {
		return keys, fmt.Errorf(
			GKFErrGroupNotFound.Error()+" - Key file does not have group '%s'",
			groupName,
		)
	}
	return keyfile.groupHash[groupName].Keys(), nil
}

func (keyfile *GKeyFile) GetGroups() ([]string, error) {
	var glist []string
	for _, group := range keyfile.groups {
		glist = append(glist, group.Name)
	}
	return glist, nil
}

func (keyfile *GKeyFile) GetValue(groupName string, key string) (interface{}, GKeyFileValueType, error) {
	/*
	 * g_key_file_get_value:
	 *
	 * Returns the parsed value associated with @key under @group_name.
	 * Use g_key_file_get_string() to retrieve an unparsed string.
	 *
	 * In the event the key cannot be found, nil is returned and
	 * @error is set to #G_KEY_FILE_ERROR_KEY_NOT_FOUND.  In the
	 * event that the @group_name cannot be found, %NULL is returned
	 * and @error is set to #G_KEY_FILE_ERROR_GROUP_NOT_FOUND.
	 */
	group := keyfile.groupHash[groupName]
	if group == nil {
		return nil, G_KEY_FILE_TYPE_STR, GKFErrGroupNotFound
	}
	k := group.LookupMap[key]
	if k == nil {
		return nil, G_KEY_FILE_TYPE_STR, GKFErrKeyNotFound
	}
	return k.Value, k.valType, nil
}

func (keyfile *GKeyFile) SetValue(groupName string, key string, value interface{}, valueType GKeyFileValueType) {
	/*
	 * g_key_file_set_value:
	 *
	 * Associates a new value with @key under @group_name.
	 *
	 * If @key cannot be found then it is created. If @group_name cannot
	 * be found then it is created.
	 *
	 * This function assumes that `value` is of the specified `valueType` -
	 * if supplying an unparsed string, use keyfile.SetValueString
	 *
	 * String values must be properly escaped (eg. `\n` instead of a newline) or
	 * the resulting keyfile may be invalid.
	 */

	// only creates a new group if a group of that name doesn't already exist
	group, _ := keyfile.AddGroup(groupName)
	k := group.LookupMap[key]
	if k == nil {
		pair := &GKeyFileKeyValuePair{
			Key:     key,
			Value:   value,
			valType: valueType,
		}
		group.AddKeyValuePair(pair)
	} else {
		group.LookupMap[key].Value = value
	}
}

func (keyfile *GKeyFile) GetString(groupName string, key string) (string, error) {
	/**
	 * g_key_file_get_string:
	 *
	 * Returns the string value associated with @key under @group_name.
	 **/
	group := keyfile.groupHash[groupName]
	if group == nil {
		return "", GKFErrGroupNotFound
	}
	k := group.LookupMap[key]
	if k == nil {
		return "", GKFErrKeyNotFound
	}
	return k.ValueAsString(keyfile.listSeparator), nil
}

func (keyfile *GKeyFile) SetString(groupName string, key string, value string) error {
	/*
	 * g_key_file_set_string:
	 *
	 * Associates a new value with @key under @group_name.
	 *
	 * If @key cannot be found then it is created. If @group_name cannot
	 * be found then it is created.
	 *
	 * This function will parse the string and set the key/value pair to the
	 * correct valueType automatically.
	 *
	 * String values must be properly escaped (eg. `\n` instead of a newline) or
	 * the resulting keyfile may be invalid.
	 */

	// only creates a new group if a group of that name doesn't already exist
	group, _ := keyfile.AddGroup(groupName)
	k := group.LookupMap[key]
	if k == nil {
		pair := &GKeyFileKeyValuePair{
			Key:   key,
			Value: value,
		}
		err := pair.ParseType(keyfile.listSeparator)
		if err != nil {
			return err
		}
		group.AddKeyValuePair(pair)
	} else {
		group.LookupMap[key].Value = value
		err := group.LookupMap[key].ParseType(keyfile.listSeparator)
		if err != nil {
			return err
		}
	}
	return nil
}

func (keyfile *GKeyFile) HasKey(groupName string, key string) bool {
	if keyfile.groupHash[groupName] != nil {
		if keyfile.groupHash[groupName].LookupMap[key] != nil {
			return true
		}
	}
	return false
}

func (keyfile *GKeyFile) FindKey(groupName string, key string) *GKeyFileKeyValuePair {
	if keyfile.groupHash[groupName] != nil {
		return keyfile.groupHash[groupName].LookupMap[key]
	}
	return nil
}

func (keyfile *GKeyFile) RemoveKey(groupName string, key string) error {
	var err error
	g := keyfile.groupHash[groupName]
	if g != nil {
		err = g.DeleteKey(key)
	}
	return err
}

func (keyfile *GKeyFile) RemoveGroup(groupName string) bool {
	g := keyfile.LookupGroup(groupName)
	if g == nil {
		// group doesn't exist, nothing to do
		return false
	}
	index := -1
	for i, v := range keyfile.groups {
		if v == g {
			// wait until we're done iterating before mucking with the slice
			index = i
			break
		}
	}
	return keyfile.deleteGroup(groupName, index)
}

func (keyfile *GKeyFile) deleteGroup(groupName string, index int) bool {
	switch index {
	case -1:
		// group isn't in the list; just delete the key from the LookupMap
	case 0:
		keyfile.groups = keyfile.groups[1:]
	case len(keyfile.groups) - 1:
		keyfile.groups = keyfile.groups[:len(keyfile.groups)-1]
	default:
		keyfile.groups = append(keyfile.groups[:index], keyfile.groups[index+1:]...)
	}
	delete(keyfile.groupHash, groupName)
	return true
}

func (keyfile *GKeyFile) SaveToFile(filename string) error {
	f, err := os.Create(filename)
	if err != nil {
		return err
	}
	defer f.Close()
	w := bufio.NewWriter(f)
	fmt.Fprint(w, keyfile.ToData())
	return w.Flush()
}
