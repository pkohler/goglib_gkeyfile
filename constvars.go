package gkeyfile

import (
	"fmt"
	"regexp"
)

type GKeyFileFlags int32
type GKeyFileError int32
type GKeyFileValueType int32

const (
	G_KEY_FILE_NONE              GKeyFileFlags = 0
	G_KEY_FILE_KEEP_COMMENTS     GKeyFileFlags = 1 << 0
	G_KEY_FILE_KEEP_TRANSLATIONS GKeyFileFlags = 1 << 1
)

const (
	g_KEY_FILE_ERROR_UNKNOWN_ENCODING GKeyFileError = iota
	g_KEY_FILE_ERROR_PARSE
	g_KEY_FILE_ERROR_NOT_FOUND
	g_KEY_FILE_ERROR_KEY_NOT_FOUND
	g_KEY_FILE_ERROR_GROUP_NOT_FOUND
	g_KEY_FILE_ERROR_INVALID_VALUE
	// not originally in glib, but used for anything not implemented in this port
	g_KEY_FILE_ERROR_NOT_IMPLEMENTED
)

/*
 * Key files can store strings (possibly with localized variants), integers,
 * booleans and lists of these. Lists are separated by a separator character,
 * typically ';' or ','. To use the list separator character in a value in
 * a list, it has to be escaped by prefixing it with a backslash.
 */
const (
	G_KEY_FILE_TYPE_STR GKeyFileValueType = iota
	G_KEY_FILE_TYPE_INT
	G_KEY_FILE_TYPE_BOOL
	G_KEY_FILE_TYPE_STR_LIST
	G_KEY_FILE_TYPE_INT_LIST
	G_KEY_FILE_TYPE_BOOL_LIST
)

var (
	GKFErrUnknownEnc = fmt.Errorf(
		"%v - G_KEY_FILE_ERROR_UNKNOWN_ENCODING",
		g_KEY_FILE_ERROR_UNKNOWN_ENCODING,
	)
	GKFErrParse = fmt.Errorf(
		"%v - G_KEY_FILE_ERROR_PARSE",
		g_KEY_FILE_ERROR_PARSE,
	)
	GKFErrNotFound = fmt.Errorf(
		"%v - G_KEY_FILE_ERROR_NOT_FOUND",
		g_KEY_FILE_ERROR_NOT_FOUND,
	)
	GKFErrKeyNotFound = fmt.Errorf(
		"%v - G_KEY_FILE_ERROR_KEY_NOT_FOUND",
		g_KEY_FILE_ERROR_KEY_NOT_FOUND,
	)
	GKFErrGroupNotFound = fmt.Errorf(
		"%v - G_KEY_FILE_ERROR_GROUP_NOT_FOUND",
		g_KEY_FILE_ERROR_GROUP_NOT_FOUND,
	)
	GKFErrInvalidValue = fmt.Errorf(
		"%v - G_KEY_FILE_ERROR_INVALID_VALUE",
		g_KEY_FILE_ERROR_INVALID_VALUE,
	)
	GKFErrNotImplemented = fmt.Errorf(
		"%v - G_KEY_FILE_ERROR_NOT_IMPLEMENTED",
		g_KEY_FILE_ERROR_NOT_IMPLEMENTED,
	)

	reGroupOrLocaleName = regexp.MustCompile(`\[(.*)\]`)
	reIsComment         = regexp.MustCompile(`^[#\n]`)
	reIsGroup           = regexp.MustCompile(`^\[.*\]\S*$`)
	reIsKeyValuePair    = regexp.MustCompile(`^.*=.*`)
	reValidLocale       = regexp.MustCompile(`[a-zA-Z\d\-_\.@]+`)

	reIsBool     = regexp.MustCompile(`^(true|false)$`)
	reIsInt      = regexp.MustCompile(`^-?\d+$`)
	reMustEscape = regexp.MustCompile(`[\.\\\{\}\[\]\^\$\-]`)
)
