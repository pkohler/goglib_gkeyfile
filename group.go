package gkeyfile

import "fmt"

type GKeyFileGroup struct {
	Name string
	/* Name will be empty for above first group (which will be comments) */
	Comment *GKeyFileKeyValuePair
	/* Special comment that is stuck to the top of a group */
	KeyValuePairs []*GKeyFileKeyValuePair
	LookupMap     map[string]*GKeyFileKeyValuePair
	/*
	 * LookupMap is used in parallel with key_value_pairs for increased lookup
	 * performance
	 */
}

func (group *GKeyFileGroup) Clear() error {
	group.Name = ""
	group.Comment = nil
	group.KeyValuePairs = []*GKeyFileKeyValuePair{}
	group.LookupMap = map[string]*GKeyFileKeyValuePair{}
	return nil
}

func (group *GKeyFileGroup) AddKeyValuePair(pair *GKeyFileKeyValuePair) error {
	group.KeyValuePairs = append(group.KeyValuePairs, pair)
	group.LookupMap[pair.Key] = pair
	return nil
}

func (group *GKeyFileGroup) String() string {
	/*
	 * Default string representation of a GKeyFileGroup.
	 * Assumes that the list seperator is the default ";"
	 */
	return group.ToString(";")
}

func (group *GKeyFileGroup) ToString(listSeparator string) string {
	ret := ""
	if group.Name != "" {
		ret += fmt.Sprintf("[%s]\n", group.Name)
	}
	for i, pair := range group.KeyValuePairs {
		ret += pair.ToString(listSeparator)
		if i < (len(group.KeyValuePairs) - 1) {
			ret += "\n"
		}
	}
	return ret
}

func (group *GKeyFileGroup) Keys() []string {
	var keys []string
	for k := range group.LookupMap {
		keys = append(keys, k)
	}
	return keys
}

func (group *GKeyFileGroup) DeleteKey(key string) error {
	k := group.LookupMap[key]
	if k == nil {
		// key doesn't exist; do nothing
		return nil
	}
	index := -1
	for i, p := range group.KeyValuePairs {
		if p == k {
			// wait until we're done iterating before mucking with the slice
			index = i
			break
		}
	}
	err := group.deletePair(key, index)
	return err
}

func (group *GKeyFileGroup) deletePair(key string, index int) error {
	i := index
	switch i {
	case -1:
		// key isn't in the list; just delete the key from the LookupMap
	case 0:
		group.KeyValuePairs = group.KeyValuePairs[1:]
	case len(group.KeyValuePairs) - 1:
		group.KeyValuePairs = group.KeyValuePairs[:len(group.KeyValuePairs)-1]
	default:
		group.KeyValuePairs = append(group.KeyValuePairs[:i], group.KeyValuePairs[i+1:]...)
	}
	delete(group.LookupMap, key)
	return nil
}
