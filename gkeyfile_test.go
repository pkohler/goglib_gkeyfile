package gkeyfile

import (
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
	"testing"
)

func TestInit(t *testing.T) {
	var g GKeyFile
	fmt.Printf("Before initialization:\n%v\n", g)
	g.Init()
	fmt.Printf("After initialization:\n%v\n", g)
	if g.flags != 0 {
		t.Errorf("flags improperly set, should be 0 but is %v", g.flags)
	}
	if g.listSeparator != ";" {
		t.Errorf("list separator improperly set, should be ';' but is %v", g.listSeparator)
	}
}

func TestClear(t *testing.T) {
	fakekvp := GKeyFileKeyValuePair{
		Key:   "foo",
		Value: "bar",
	}
	fakegroup := GKeyFileGroup{
		Name:          "fake",
		Comment:       &fakekvp,
		KeyValuePairs: []*GKeyFileKeyValuePair{&fakekvp},
		LookupMap:     map[string]*GKeyFileKeyValuePair{"foo": &fakekvp},
	}
	g := GKeyFile{
		groups:        []*GKeyFileGroup{&fakegroup},
		groupHash:     map[string]*GKeyFileGroup{"fake": &fakegroup},
		startGroup:    &fakegroup,
		currentGroup:  &fakegroup,
		parseBuffer:   "something",
		listSeparator: ";;",
		flags:         G_KEY_FILE_KEEP_COMMENTS,
		locales:       []string{"EN"},
	}
	fmt.Printf("Before clearing:\n%v\n", g)
	g.Clear()
	fmt.Printf("After clearing:\n%v\n", g)
	if len(g.groups) != 0 {
		t.Errorf("Groups didn't clear; found %v", g.groups)
	}
	if g.groupHash["fake"] != nil {
		t.Errorf("GroupHash didn't clear; found %v", g.groupHash["fake"])
	}
	if g.startGroup.Name != "" {
		t.Errorf("StartGroup didn't clear, found %v", *g.startGroup)
	}
	if g.currentGroup.Name != "" {
		t.Errorf("CurrentGroup didn't clear, found %v", *g.currentGroup)
	}
	if g.parseBuffer != "" {
		t.Errorf("ParseBuffer didn't clear, found %v", g.parseBuffer)
	}
	if g.listSeparator != ";" {
		t.Errorf("ListSeparator didn't clear, found %v", g.listSeparator)
	}
	if g.flags != G_KEY_FILE_NONE {
		t.Errorf("Flags didn't clear, set to %v", g.flags)
	}
	if len(g.locales) != 0 {
		t.Errorf("Locales didn't clear, found %v", g.locales)
	}
}

func TestFindFile(t *testing.T) {
	info, path, err := findFileInDataDirs("gkeyfile_test.go", []string{"."})
	fmt.Printf("info: %v\npath: %s\n", info, path)
	if err != nil {
		t.Errorf("failed to find file with error %s", err.Error())
	}
}

func TestNewAndSetSep(t *testing.T) {
	g := NewGKeyFile()
	if g.listSeparator != ";" {
		t.Errorf("keyfile did not initialize properly; current values are:\n%v\n", g)
	}
	g.SetListSeperator("|")
	if g.listSeparator != "|" {
		t.Errorf("list separator did not set to |; still shows as %s", g.listSeparator)
	}
}

func TestLanguageAcquisition(t *testing.T) {
	lang, err := GGetLanguageNames()
	fmt.Printf("languages detected: %v\n", strings.Split(lang, " "))
	if err != nil {
		t.Errorf("Error receieved when parsing languages:\n %s", err.Error())
	}
}

func TestWalk(t *testing.T) {
	contents, err := walkDir(".")
	if err != nil {
		fmt.Println("Directory walk results:")
		for _, i := range contents {
			fmt.Println(filepath.Join(i[0], i[1]))
		}
		t.Errorf("Error when walking directory:\n %s", err.Error())
	}
}

func TestLoadFromFD(t *testing.T) {
	g := NewGKeyFile()
	fd, _ := os.Lstat(".")
	err := g.LoadFromFD(fd, 0)
	if err != GKFErrNotImplemented {
		t.Errorf("LoadFromFD should have thrown GKFErrNotImplemented, got %v", err.Error())
	}
}

func TestLoadFromFileAndParse(t *testing.T) {
	g := NewGKeyFile()
	// test keeping comments and translations
	err := g.LoadFromFile("examples/example.keyfile", G_KEY_FILE_KEEP_COMMENTS|G_KEY_FILE_KEEP_TRANSLATIONS)
	if err != nil {
		t.Errorf("LoadFromFile keeping comments and translations failed with error:\n%v", err.Error())
	}
	if g.groupHash["First Group"] == nil {
		t.Errorf(
			"keyfile didn't load 'First Group' group into hashtable"+
				"\ngroupHash:\n\t%v",
			g.groupHash,
		)
	} else if len(g.groupHash["First Group"].KeyValuePairs) != 11 {
		kvpstr := ""
		for _, kvp := range g.groupHash["First Group"].KeyValuePairs {
			kvpstr += fmt.Sprintf("\t%v\n", kvp)
		}
		t.Errorf(
			"Failed to load all key/value pairs, comments and translations into 'First Group':\n%s",
			kvpstr,
		)
	}
	if g.groupHash["Another Group"] == nil {
		t.Errorf(
			"keyfile didn't load 'Another Group' group into hashtable"+
				"\ngroupHash:\n\t%v",
			g.groupHash,
		)
	} else if len(g.groupHash["Another Group"].KeyValuePairs) != 4 {
		kvpstr := ""
		for _, kvp := range g.groupHash["Another Group"].KeyValuePairs {
			kvpstr += fmt.Sprintf("\t%v\n", kvp)
		}
		t.Errorf(
			"Failed to load all key/value pairs, comments and translations into 'Another Group':\n%s",
			kvpstr,
		)
	}
	// test with only comments
	g.Clear()
	err = g.LoadFromFile("examples/example.keyfile", G_KEY_FILE_KEEP_COMMENTS)
	if err != nil {
		t.Errorf("LoadFromFile keeping comments only failed with error:\n%v", err.Error())
	}
	if g.groupHash["First Group"] == nil {
		t.Errorf(
			"keyfile didn't load 'First Group' group into hashtable"+
				"\ngroupHash:\n\t%v",
			g.groupHash,
		)
	} else if len(g.groupHash["First Group"].KeyValuePairs) != 6 {
		kvpstr := ""
		for _, kvp := range g.groupHash["First Group"].KeyValuePairs {
			kvpstr += fmt.Sprintf("\t%v\n", kvp)
		}
		t.Errorf(
			"Failed to load all/only key/value pairs and comments into 'First Group':\n%s",
			kvpstr,
		)
	}
	if g.groupHash["Another Group"] == nil {
		t.Errorf(
			"keyfile didn't load 'Another Group' group into hashtable"+
				"\ngroupHash:\n\t%v",
			g.groupHash,
		)
	} else if len(g.groupHash["Another Group"].KeyValuePairs) != 4 {
		kvpstr := ""
		for _, kvp := range g.groupHash["Another Group"].KeyValuePairs {
			kvpstr += fmt.Sprintf("\t%v\n", kvp)
		}
		t.Errorf(
			"Failed to load all/only key/value pairs and comments 'Another Group':\n%s",
			kvpstr,
		)
	}
	// test with only key/value pairs
	g.Clear()
	err = g.LoadFromFile("examples/example.keyfile", G_KEY_FILE_NONE)
	if err != nil {
		t.Errorf("LoadFromFile keeping key/value pairs only failed with error:\n%v", err.Error())
	}
	if g.groupHash["First Group"] == nil {
		t.Errorf(
			"keyfile didn't load 'First Group' group into hashtable"+
				"\ngroupHash:\n\t%v",
			g.groupHash,
		)
	} else if len(g.groupHash["First Group"].KeyValuePairs) != 2 {
		kvpstr := ""
		for _, kvp := range g.groupHash["First Group"].KeyValuePairs {
			kvpstr += fmt.Sprintf("\t%v\n", kvp)
		}
		t.Errorf(
			"Failed to load only key/value pairs into 'First Group':\n%s",
			kvpstr,
		)
	}
	if g.groupHash["Another Group"] == nil {
		t.Errorf(
			"keyfile didn't load 'Another Group' group into hashtable"+
				"\ngroupHash:\n\t%v",
			g.groupHash,
		)
	} else if len(g.groupHash["Another Group"].KeyValuePairs) != 2 {
		kvpstr := ""
		for _, kvp := range g.groupHash["Another Group"].KeyValuePairs {
			kvpstr += fmt.Sprintf("\t%v\n", kvp)
		}
		t.Errorf(
			"Failed to load only key/value pairs 'Another Group':\n%s",
			kvpstr,
		)
	}
}

func TestLoadFromDataAndToData(t *testing.T) {
	filebytes, err := ioutil.ReadFile("examples/example.keyfile")
	if err != nil {
		t.Errorf("Error loading examples/example.keyfile:\n%v", err.Error())
	}
	filestr := string(filebytes)
	g := NewGKeyFile()
	err = g.LoadFromData(filestr, G_KEY_FILE_KEEP_COMMENTS|G_KEY_FILE_KEEP_TRANSLATIONS)
	if err != nil {
		t.Errorf("Error loading keyfile from string:\n%v", err.Error())
	}
	if g.ToData() != filestr {
		t.Errorf(
			"Mismatch between loaded and saved data.\n\n"+
				"__LOADED KEYFILE__\n%v\n\n__SAVED KEYFILE__\n%v",
			filestr,
			g.ToData(),
		)
	}
	err = g.SaveToFile("examples/example.keyfile")
	if err != nil {
		t.Errorf("Error saveing to examples/example.keyfile: %s", err.Error())
	}
}

func TestLoadFromDirs(t *testing.T) {
	g := NewGKeyFile()
	err := g.LoadFromDirs("example.keyfile", []string{"."}, G_KEY_FILE_NONE)
	if err != nil {
		t.Errorf("Error loading example.keyfile from dir search:\n%v", err.Error())
	}
}

func TestParseTypes(t *testing.T) {
	g := NewGKeyFile()
	g.LoadFromFile("examples/example.keyfile", G_KEY_FILE_NONE)
	// test single string
	val, typ, err := g.GetValue("First Group", "Name")
	if err != nil {
		t.Errorf("GetValue for 'First Group', 'Name' threw error: %s", err.Error())
	}
	if typ != G_KEY_FILE_TYPE_STR {
		t.Errorf("Incorrect type entry for Name, should be %v but was %v", G_KEY_FILE_TYPE_STR, typ)
	}
	if val.(string) != "Key File Example\\tthis value shows\\nescaping" {
		t.Errorf("Name has incorrect string value; found: %s", val.(string))
	}
	// test int list
	val, typ, err = g.GetValue("Another Group", "Numbers")
	if err != nil {
		t.Errorf("GetValue for 'Another Group', 'Numbers' threw error: %s", err.Error())
	}
	if typ != G_KEY_FILE_TYPE_INT_LIST {
		t.Errorf("Incorrect type entry for Numbers, should be %v but was %v", G_KEY_FILE_TYPE_INT_LIST, typ)
	}
	a := []int64{2, 20, -200, 0}
	b := val.([]int64)
	for i, _ := range a {
		if a[i] != b[i] {
			t.Errorf("Numbers has incorrect []int64 value; found: %v", b)
		}
	}
	// test bool list
	val, typ, err = g.GetValue("Another Group", "Booleans")
	if err != nil {
		t.Errorf("GetValue for 'Another Group', 'Booleans' threw error: %s", err.Error())
	}
	if typ != G_KEY_FILE_TYPE_BOOL_LIST {
		t.Errorf("Incorrect type entry for Booleans, should be %v but was %v", G_KEY_FILE_TYPE_BOOL_LIST, typ)
	}
	c := []bool{true, false, true, true}
	d := val.([]bool)
	for i, _ := range c {
		if c[i] != d[i] {
			t.Errorf("Booleans has incorrect []bool value; found: %v", c)
		}
	}
}

func TestAdd(t *testing.T) {
	g := NewGKeyFile()
	g.LoadFromFile("examples/example.keyfile", G_KEY_FILE_NONE)
	_, err := g.AddGroup("First Group")
	if err != nil {
		t.Errorf("Received error on AddGroup('First Group'): %s", err.Error())
	}
	_, err = g.AddGroup("")
	if err != GKFErrInvalidValue {
		t.Error("Should have received invalid value err when trying to add a blank groupname")
	}
}

func TestGetKeys(t *testing.T) {
	g := NewGKeyFile()
	g.LoadFromFile("examples/example.keyfile", G_KEY_FILE_KEEP_TRANSLATIONS)
	keys, err := g.GetKeys("Another Group")
	if err != nil {
		t.Errorf("GetKeys threw error: %s", err.Error())
	}
	if len(keys) != 2 {
		t.Errorf("Found %d keys in 'Another Group'; should have been 2", len(keys))
	}
	keys, err = g.GetKeys("First Group")
	if err != nil {
		t.Errorf("GetKeys threw error: %s", err.Error())
	}
	if len(keys) != 7 {
		t.Errorf("Found %d keys in 'First Group'; should have been 7", len(keys))
	}
	_, err = g.GetKeys("Invalid Group")
	if err == nil {
		t.Error("Should have received an error trying to get keys for a non-existant group")
	}
}

func TestGetSetValue(t *testing.T) {
	g := NewGKeyFile()
	g.LoadFromFile("examples/example.keyfile", G_KEY_FILE_KEEP_TRANSLATIONS)
	_, _, err := g.GetValue("badgroup", "somekey")
	if err != GKFErrGroupNotFound {
		t.Error("Should have received GroupNotFound error")
	}
	_, _, err = g.GetValue("First Group", "Unwelcome")
	if err != GKFErrKeyNotFound {
		t.Error("Should have received KeyNotFound error")
	}
	g.SetValue("New Group", "Unwelcome", "suck an egg", G_KEY_FILE_TYPE_STR)
	g.SetValue("New Group", "AllFalse", []bool{false, false}, G_KEY_FILE_TYPE_BOOL_LIST)
	s, err := g.GetString("New Group", "AllFalse")
	if err != nil {
		t.Errorf("Error loading AllFalse as a string: %s", err.Error())
	}
	if s != "false;false" {
		t.Errorf("Failed to properly stringify AllFalse, got %s", s)
	}
	err = g.SetString("New Group", "SomeInts", "1;2;-3;2048")
	if err != nil {
		t.Errorf("Error setting SomeInts: %s", err.Error())
	}
	someInts, typ, err := g.GetValue("New Group", "SomeInts")
	if err != nil {
		t.Errorf("Error getting SomeInts: %s", err.Error())
	}
	if typ != G_KEY_FILE_TYPE_INT_LIST {
		t.Errorf("SomeInts should be G_KEY_FILE_TYPE_INT_LIST; got %v", typ)
	}
	_, ok := someInts.([]int64)
	if !ok {
		t.Errorf("Unable to convert SomeInts to []int64; their value was %v", someInts)
	}
	err = g.SetString("New Group", "SomeInts", "-1;-7;42")
	if err != nil {
		t.Errorf("Error re-setting SomeInts: %s", err.Error())
	}
}

func TestInterestingLocales(t *testing.T) {
	g := NewGKeyFile()
	g.LoadFromFile("examples/example.keyfile", G_KEY_FILE_KEEP_TRANSLATIONS)
	g.LoadFromFile("examples/example.keyfile", G_KEY_FILE_NONE)
	g.locales = append(g.locales, "en")
	if !g.localeIsInteresting("en") {
		t.Error("English should be counted as an interesting language, but wasn't")
	}
}

func TestGetGroups(t *testing.T) {
	g := NewGKeyFile()
	g.LoadFromFile("examples/example.keyfile", G_KEY_FILE_NONE)
	groups, err := g.GetGroups()
	if err != nil {
		t.Errorf("GetGroups threw error: %s", err.Error())
	}
	if len(groups) != 2 {
		t.Errorf("GetGroups found %d groups; should have been 2", len(groups))
	}
}

func TestDelete(t *testing.T) {
	g := NewGKeyFile()
	g.LoadFromFile("examples/example.keyfile", G_KEY_FILE_KEEP_TRANSLATIONS)
	// test RemoveKey
	keylen := len(g.groupHash["First Group"].KeyValuePairs)
	err := g.RemoveKey("First Group", "Welcome")
	if err != nil {
		t.Errorf("Error on RemoveKey: %s", err.Error())
	}
	newkeylen := len(g.groupHash["First Group"].KeyValuePairs)
	if newkeylen >= keylen {
		t.Error("KeyValuePairs didn't decrement properly on RemoveKey")
	}
	if g.groupHash["First Group"].LookupMap["Welcome"] != nil {
		t.Error("LookupMap still contains 'Welcome' key after RemoveKey")
	}
	// test RemoveGroup
	grouplen := len(g.groups)
	removed := g.RemoveGroup("First Group")
	if !removed {
		t.Error("Couldn't remove 'First Group' - odd")
	}
	if grouplen-len(g.groups) != 1 {
		t.Error("Group listing didn't decrement properly on RemoveGroup")
	}
	if g.groupHash["First Group"] != nil {
		t.Error("'First Group' didn't get removed from the lookup hash correctly")
	}
	// test other cases for RemoveGroup
	g.AddGroup("one")
	g.AddGroup("two")
	g.AddGroup("three")
	removed = g.RemoveGroup("one")
	if !removed {
		t.Error("Couldn't remove 'one'")
	}
	removed = g.RemoveGroup("three")
	if !removed {
		t.Error("Couldn't remove 'three'")
	}
}

func TestBadFile(t *testing.T) {
	g := NewGKeyFile()
	err := g.LoadFromDataDirs("example.badkeyfile.1", G_KEY_FILE_NONE)
	if err != GKFErrNotImplemented {
		t.Error("LoadFromDataDirs should not be implemented... what's happening?")
	}
	flags := G_KEY_FILE_KEEP_COMMENTS | G_KEY_FILE_KEEP_TRANSLATIONS
	err = g.LoadFromFile("example.badkeyfile.1", flags)
	if err == nil {
		t.Error("LoadFromFile should have caught the invalid placement of the first key-value pair")
	}
	err = g.LoadFromFile("example.badkeyfile.2", flags)
	if err == nil {
		t.Error("LoadFromFile should have caught the lines that are not group/kvp/comments")
	}
	err = g.LoadFromFile("example.badkeyfile.3", flags)
	if err == nil {
		t.Error("[] should have been caught as an invalid group name")
	}
	err = g.LoadFromFile("example.badkeyfile.4", flags)
	if err == nil {
		t.Error("Keys shouldn't be allowed to have trailing spaces")
	}
}

func TestSearch(t *testing.T) {
	g := NewGKeyFile()
	g.LoadFromFile("examples/example.keyfile", G_KEY_FILE_KEEP_TRANSLATIONS)
	found := g.HasKey("First Group", "Welcome")
	if !found {
		t.Error("Unable to find valid key")
	}
	found = g.HasKey("First Group", "Unwelcome")
	if found {
		t.Error("Found invalid key somehow")
	}
	k := g.FindKey("First Group", "Welcome")
	if k == nil {
		t.Error("Unable to load valid key")
	}
	k = g.FindKey("First Group", "Unwelcome")
	if k != nil {
		t.Error("Loaded invalid key somehow")
	}
}
